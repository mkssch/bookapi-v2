FROM php:7.2-apache

ENV APACHE_DOCUMENT_ROOT /app/public

RUN a2enmod rewrite

RUN apt-get update && apt-get install -y libpq-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install \
               pdo \
               pdo_pgsql \
               pgsql

RUN pecl install xdebug-2.8.1 && docker-php-ext-enable xdebug

COPY ./conf.d /usr/local/etc/php/conf.d

RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

WORKDIR /app