<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use App\Types\OrderStatus;
use App\Types\Phone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="orders")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * @var Phone
     * @ORM\Column(type="phone_type", length=50, nullable=false)
     */
    private $phone;

    /**
     * @var OrderStatus
     * @ORM\Column(type="order_status_type", nullable=false)
     */
    private $status;

    /**
     * @ORM\ManyToMany(targetEntity=Book::class)
     */
    private $bookList;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=false, name="created_at")
     */
    private $createdAt;

    public function __construct()
    {
        $this->bookList = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable('now');
    }

    public static function newOrder(): self
    {
        $self = new self();
        $self->setStatus(OrderStatus::pendingStatus());
        return $self;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): Phone
    {
        return $this->phone;
    }

    public function setPhone(Phone $phone): void
    {
        $this->phone = $phone;
    }

    public function getStatus(): OrderStatus
    {
        return $this->status;
    }

    public function setStatus(OrderStatus $status): void
    {
        $this->status = $status;
    }

    /**
     * @return Collection|Book[]
     */
    public function getBookList(): Collection
    {
        return $this->bookList;
    }

    public function addBook(Book $book): self
    {
        if (!$this->bookList->contains($book)) {
            $this->bookList[] = $book;
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        $this->bookList->removeElement($book);

        return $this;
    }

    public function setProcessed(): void
    {
        $this->status = $this->status->setProcessed();
    }

    public function setDelivered(): void
    {
        $this->status = $this->status->setDelivered();
    }

    public function setDeclined(): void
    {
        $this->status = $this->status->setDeclined();
    }

    public function isPending(): bool
    {
        return $this->status->isPending();
    }

    public function isProcessed(): bool
    {
        return $this->status->isProcessed();
    }

    public function isDelivered(): bool
    {
        return $this->status->isDelivered();
    }

    public function isDeclined(): bool
    {
        return $this->status->isDeclined();
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
