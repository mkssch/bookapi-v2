<?php

namespace App\UseCase\Book\Create;

use App\Validators\AuthorConstraint;
use Symfony\Component\Validator\Constraints as Assert;

class BookCreateCommand
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    public $title;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    public $description;

    /**
     * @var array
     * @Assert\NotBlank()
     * @AuthorConstraint()
     */
    public $author_ids;
}