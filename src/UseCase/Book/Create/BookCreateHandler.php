<?php

namespace App\UseCase\Book\Create;

use App\Entity\Author;
use App\Entity\Book;
use App\Types\Status;
use Doctrine\ORM\EntityManagerInterface;

class BookCreateHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function handle(BookCreateCommand $bookCreateCommand, array $authors) : Book
    {
        $book = new Book(new Status(Status::STATUS_ACTIVE));
        $book->setTitle($bookCreateCommand->title);
        $book->setDescription($bookCreateCommand->description);
        foreach ($authors as $author){
            $book->addAuthor($author);
        }
        $this->manager->persist($book);
        $this->manager->flush();
        return $book;
    }
}