<?php


namespace App\UseCase\Book\Search;


use App\Entity\Book;
use App\Repository\BookRepository;
use http\Exception\InvalidArgumentException;

class BookSearchHandler
{
    /**
     * @var BookRepository
     */
    private $repository;

    public function __construct(BookRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param BookSearchCommand $command
     * @return Book[]
     */
    public function handle(BookSearchCommand $command): array
    {
        $query = $this->repository->createQueryBuilder('b');
        if (!is_null($command->title)) {
            $query->andWhere('b.title LIKE :title')->setParameter('title', "%{$command->title}%");
        }
        if (!is_null($command->description)) {
            $query->andWhere('b.description LIKE :description')->setParameter('description', "%{$command->description}%");
        }
        if (!is_null($command->status)) {
            $query->andWhere('b.status = :status')->setParameter('status', $command->status);
        }
        if (!is_null($command->authorName)){
            $query->join('b.authors', 'author')
                ->andWhere('author.name LIKE :authorName')
                ->setParameter('authorName', "%{$command->authorName}%");
        }
        return $query->getQuery()->execute();
    }

}