<?php


namespace App\UseCase\Book\Search;


use App\Validators\StatusConstraint;
use Symfony\Component\Validator\Constraints as Assert;

class BookSearchCommand
{
    /**
     * @var string
     * @Assert\Length(max=50)
     */
    public $title;

    /**
     * @var string
     * @Assert\Length(max=50)
     */
    public $description;

    /**
     * @var int
     * @StatusConstraint()
     */
    public $status;


    /**
     * @var string
     * @Assert\Length(max="50")
     */
    public $authorName;

    public function getAuthorName(): ?string
    {
        return $this->authorName;
    }

    /**
     * @param string $authorName
     */
    public function setAuthorName(string $authorName): void
    {
        $this->authorName = $authorName;
    }
}