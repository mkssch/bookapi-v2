<?php

namespace App\UseCase\Book\Edit;

use App\Validators\AuthorConstraint;
use App\Validators\StatusConstraint;
use Symfony\Component\Validator\Constraints as Assert;

class BookEditCommand
{
    /**
     * @var string
     * @Assert\Length(max=255)
     */
    public $title;

    /**
     * @var string
     * @Assert\Length(max=255)
     */
    public $description;

    /**
     * @var array
     * @AuthorConstraint()
    */
    public $add_authors = [];

    /**
     * @var array
     * @AuthorConstraint()
     */
    public $remove_authors = [];

    /**
     * @var integer
     * @StatusConstraint
     */
    public $status;
}