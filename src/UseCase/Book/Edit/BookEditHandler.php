<?php

namespace App\UseCase\Book\Edit;

use App\Entity\Author;
use App\Entity\Book;
use App\Repository\AuthorRepository;
use App\Types\Status;
use Doctrine\ORM\EntityManagerInterface;

class BookEditHandler
{
    /**
     * @var AuthorRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(AuthorRepository $repository, EntityManagerInterface $manager)
    {
        $this->repository = $repository;
        $this->manager = $manager;
    }

    public function handle(BookEditCommand $bookEditCommand, Book $book) : void
    {
        if (!empty($bookEditCommand->title)){
            $book->setTitle($bookEditCommand->title);
        }
        if (!empty($bookEditCommand->description)){
            $book->setDescription($bookEditCommand->description);
        }
        if (count($bookEditCommand->add_authors) > 0){
            array_map(function (Author $author) use ($book){
                $book->addAuthor($author);
            }, $this->repository->findBy(['id' => $bookEditCommand->add_authors]));
        }
        if (count($bookEditCommand->remove_authors) > 0){
            array_map(function (Author $author) use ($book){
                $book->removeAuthor($author);
            }, $this->repository->findBy(['id' => $bookEditCommand->remove_authors]));
        }
        if (!is_null($bookEditCommand->status)){
            switch ($bookEditCommand->status){
                case Status::STATUS_ACTIVE:
                    $book->setActive();
                    break;
                case Status::STATUS_INACTIVE:
                    $book->setInactive();
                    break;
            }
        }
        $this->manager->flush();
    }
}