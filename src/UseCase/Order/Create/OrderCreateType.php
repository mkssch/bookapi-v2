<?php


namespace App\UseCase\Order\Create;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderCreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('address', TextType::class)
            ->add('phone', TextType::class)
            ->add('bookList', CollectionType::class, ['allow_add' => true, 'entry_type' => IntegerType::class]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OrderCreateCommand::class,
            'csrf_protection' => false,
            'method' => 'POST',
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}