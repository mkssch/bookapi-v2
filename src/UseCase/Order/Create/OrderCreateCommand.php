<?php


namespace App\UseCase\Order\Create;


use App\Types\Phone;
use App\Validators\PhoneConstraint;
use Symfony\Component\Validator\Constraints as Assert;

class OrderCreateCommand
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    public $address;

    /**
     * @var string
     * @Assert\Length(max="50")
     * @PhoneConstraint()
     */
    public $phone;

    /**
     * @var array
     * @Assert\NotBlank()
     * @Assert\Collection(fields={}, allowExtraFields=true)
     */
    public $bookList;

    public function getPhoneObject(): Phone
    {
        return new Phone($this->phone);
    }

}