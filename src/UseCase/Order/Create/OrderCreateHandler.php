<?php


namespace App\UseCase\Order\Create;


use App\Entity\Book;
use App\Entity\Order;
use App\Repository\BookRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderCreateHandler
{
    /**
     * @var BookRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    public function __construct(EntityManagerInterface $manager, BookRepository $repository)
    {
        $this->repository = $repository;
        $this->manager = $manager;
    }

    public function handle(OrderCreateCommand $command) : Order{
        $order = Order::newOrder();
        $order->setAddress($command->address);
        $order->setPhone($command->getPhoneObject());
        array_map(function (Book $book) use ($order){
            $order->addBook($book);
        }, $this->repository->findBy(['id' => $command->bookList]));
        $this->manager->persist($order);
        $this->manager->flush();
        return $order;
    }
}