<?php


namespace App\Validators;


use App\Repository\AuthorRepository;
use App\Types\Status;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Webmozart\Assert\Assert;

class StatusValidator extends ConstraintValidator
{
    /**
     * @param mixed $value
     * @param StatusConstraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        Assert::isInstanceOf($constraint, StatusConstraint::class);
        if (is_integer($value)){
            try {
                Assert::inArray($value, [Status::STATUS_ACTIVE, Status::STATUS_INACTIVE]);
            } catch (\Throwable $e){
                $this->context->addViolation($e->getMessage());
            }
        }
    }
}