<?php


namespace App\Validators;


use App\Repository\AuthorRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Webmozart\Assert\Assert;

class AuthorValidator extends ConstraintValidator
{
    /**
     * @var AuthorRepository
     */
    private $manager;

    public function __construct(AuthorRepository $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param mixed $value
     * @param AuthorConstraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        Assert::isInstanceOf($constraint, AuthorConstraint::class);
        foreach ($value as $authorID){
            $author = $this->manager->findOneBy(['id' => $authorID]);
            if (empty($author)){
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{author}}', $authorID)
                    ->addViolation();
            }
        }
    }
}