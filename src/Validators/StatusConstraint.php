<?php

namespace App\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class StatusConstraint extends Constraint
{
    public function validatedBy()
    {
        return StatusValidator::class;
    }

}