<?php

namespace App\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class AuthorConstraint extends Constraint
{
    public $message = "Doesn't exist author by id {{author}}";

    public function validatedBy()
    {
        return AuthorValidator::class;
    }

}