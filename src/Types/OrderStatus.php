<?php


namespace App\Types;


use Webmozart\Assert\Assert;

class OrderStatus
{
    const STATUS_PENDING = 0;
    const STATUS_PROCESSED = 1;
    const STATUS_DELIVERED = 2;
    const STATUS_DECLINED = 3;

    /**
     * @var int
     */
    private $status;

    /**
     * @param int $status
     */
    public function __construct(int $status)
    {
        Assert::inArray($status, self::getStatuses());
        $this->status = $status;
    }

    public static function getStatuses(): array
    {
        return [self::STATUS_PENDING, self::STATUS_PROCESSED, self::STATUS_DELIVERED, self::STATUS_DECLINED];
    }

    public static function pendingStatus(): self
    {
        return new self(self::STATUS_PENDING);
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getLabel(): string
    {
        switch ($this->status) {
            case self::STATUS_PENDING:
                return 'Pending';
            case self::STATUS_PROCESSED:
                return 'Processed';
            case self::STATUS_DELIVERED:
                return 'Delivered';
            case self::STATUS_DECLINED:
                return 'Declined';
            default:
                throw new \InvalidArgumentException('Incorrect status');
        }
    }

    public function setProcessed(): self
    {
        Assert::same($this->status, self::STATUS_PENDING);
        return new self(self::STATUS_PROCESSED);
    }

    public function setDelivered(): self
    {
        Assert::same($this->status, self::STATUS_PROCESSED);
        return new self(self::STATUS_DELIVERED);
    }

    public function setDeclined(): self
    {
        Assert::notSame($this->status, self::STATUS_DELIVERED);
        return new self(self::STATUS_DECLINED);
    }

    public function isPending(): bool
    {
        return $this->status === self::STATUS_PENDING;
    }

    public function isProcessed(): bool
    {
        return $this->status === self::STATUS_PROCESSED;
    }

    public function isDelivered(): bool
    {
        return $this->status === self::STATUS_DELIVERED;
    }

    public function isDeclined(): bool
    {
        return $this->status === self::STATUS_DECLINED;
    }
}