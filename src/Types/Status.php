<?php


namespace App\Types;


use Webmozart\Assert\Assert;

class Status
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    /**
     * @var int
     */
    private $status;

    /**
     * Status constructor.
     * @param int $status
     */
    public function __construct(int $status)
    {
        Assert::inArray($status, [self::STATUS_INACTIVE, self::STATUS_ACTIVE]);
        $this->status = $status;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function isInactive(): bool
    {
        return $this->status === self::STATUS_INACTIVE;
    }
}