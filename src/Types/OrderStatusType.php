<?php


namespace App\Types;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\IntegerType;

class OrderStatusType extends IntegerType
{
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value instanceof OrderStatus ? $value->getStatus() : $value;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new OrderStatus($value);
    }

    public function getName()
    {
        return 'order_status_type';
    }
}