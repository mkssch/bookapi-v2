<?php


namespace App\DTO;


use App\Types\OrderStatus;
use App\Types\Phone;

class OrderDTO
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $address;
    /**
     * @var Phone
     */
    private $phone;
    /**
     * @var OrderStatus
     */
    private $status;
    /**
     * @var array
     */
    private $bookList;

    /**
     * OrderDTO constructor.
     * @param int $id
     * @param string $address
     * @param Phone $phone
     * @param OrderStatus $status
     * @param BookDTO[] $bookList
     */
    public function __construct(int $id, string $address, Phone $phone, OrderStatus $status, array $bookList)
    {
        $this->id = $id;
        $this->address = $address;
        $this->phone = $phone;
        $this->status = $status;
        $this->bookList = $bookList;
    }

    public function getArrayData(): array
    {
        return [
            'id' => $this->id,
            'address' => $this->address,
            'phone' => $this->phone->getPhone(),
            'status' => $this->status->getLabel(),
            'bookList' => array_map(function (BookDTO $book) {
                return $book->getArrayData();
            }, $this->bookList)
        ];
    }
}