<?php

namespace App\DTO;

use App\Types\Status;

class BookDTO
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;
    /**
     * @var AuthorDTO[]
     */
    private $authors;
    /**
     * @var int
     */
    private $id;
    /**
     * @var Status
     */
    private $status;

    /**
     * @param int $id
     * @param string $title
     * @param string $description
     * @param array $authors
     * @param Status $status
     */
    public function __construct(int $id, string $title, string $description, array $authors, Status $status)
    {
        $this->title = $title;
        $this->description = $description;
        $this->authors = $authors;
        $this->id = $id;
        $this->status = $status;
    }

    public function getArrayData(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'active' => $this->isActive(),
            'authors' => array_map(function (AuthorDTO $authorDTO) {
                return [
                    'id' => $authorDTO->getId(),
                    'name' => $authorDTO->getName()
                ];
            }, $this->authors)
        ];
    }

    public function isActive(): bool
    {
        return $this->status->isActive();
    }
}