<?php

namespace App\Controller;

use App\DTO\AuthorDTO;
use App\DTO\BookDTO;
use App\Entity\Author;
use App\Entity\Book;
use App\Repository\AuthorRepository;
use App\UseCase\Book\Create\BookCreateCommand;
use App\UseCase\Book\Create\BookCreateHandler;
use App\UseCase\Book\Create\BookCreateType;
use App\UseCase\Book\Edit\BookEditCommand;
use App\UseCase\Book\Edit\BookEditHandler;
use App\UseCase\Book\Edit\BookEditType;
use App\UseCase\Book\Search\BookSearchCommand;
use App\UseCase\Book\Search\BookSearchHandler;
use App\UseCase\Book\Search\BookSearchType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class BookController extends AbstractController
{
    /**
     * @Route("/book", methods={"GET"})
     * @param Request $request
     * @param BookSearchHandler $handler
     * @return Response
     */
    public function index(Request $request, BookSearchHandler $handler): Response
    {
        $bookSearchCommand = new BookSearchCommand();
        $form = $this->createForm(BookSearchType::class, $bookSearchCommand);
        if (\count($request->query->all()) === 0) {
            $valid = true;
        } else {
            $form->handleRequest($request);
            $valid = $form->isSubmitted() && $form->isValid();
        }
        if ($valid) {
            $jsonData = [];
            foreach ($handler->handle($bookSearchCommand) as $book) {
                $authors = $book->getAuthors()->map(
                    function (Author $author) {
                        return new AuthorDTO($author->getId(), $author->getName());
                    }
                )->getValues();
                $dto = new BookDTO(
                    $book->getId(),
                    $book->getTitle(),
                    $book->getDescription(),
                    $authors,
                    $book->getStatus()
                );
                $jsonData[] = $dto->getArrayData();
            }
            return $this->json([
                'error' => false,
                'data' => $jsonData
            ]);
        }

        $errors = [];
        foreach ($form->getErrors(true) as $error) {
            $errors[] = [$error->getOrigin()->getName() => $error->getMessage()];
        }
        return $this->json(
            [
                'error' => true,
                'errors' => $errors
            ]
        );
    }

    /**
     * @Route("/book", methods={"POST"})
     * @param Request $request
     * @param BookCreateHandler $handler
     * @param AuthorRepository $authorRepository
     * @return Response
     */
    public function create(Request $request, BookCreateHandler $handler, AuthorRepository $authorRepository): Response
    {
        $bookCreateCommand = new BookCreateCommand();
        $form = $this->createForm(BookCreateType::class, $bookCreateCommand);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $book = $handler->handle($bookCreateCommand, $authorRepository->findBy(['id' => $bookCreateCommand->author_ids]));
                $dto = new BookDTO(
                    $book->getId(),
                    $book->getTitle(),
                    $book->getDescription(),
                    $book->getAuthors()->map(
                        function (Author $author) {
                            return new AuthorDTO($author->getId(), $author->getName());
                        }
                    )->getValues(),
                    $book->getStatus()
                );
                return $this->json([
                    'error' => false,
                    'object' => $dto->getArrayData(),
                ], Response::HTTP_CREATED);
            } catch (\Throwable $exception) {
                return $this->json(
                    [
                        'error' => true,
                        'errors' => $exception->getMessage()
                    ],
                    Response::HTTP_BAD_REQUEST
                );
            }
        }

        $errors = [];
        foreach ($form->getErrors(true) as $error) {
            $errors[] = [$error->getOrigin()->getName() => $error->getMessage()];
        }
        return $this->json(
            [
                'error' => true,
                'errors' => $errors
            ]
        );
    }

    /**
     * @Route("/book/{id}", methods={"GET"})
     * @param int $id
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function show(int $id, EntityManagerInterface $manager): Response
    {
        $book = $manager->find(Book::class, $id);
        if ($book){
            $dto = new BookDTO(
                $book->getId(),
                $book->getTitle(),
                $book->getDescription(),
                $book->getAuthors()->map(
                    function (Author $author) {
                        return new AuthorDTO($author->getId(), $author->getName());
                    }
                )->getValues(),
                $book->getStatus()
            );
            return $this->json($dto->getArrayData());
        } else {
            return $this->json([
                'error' => true,
                'message' => "Couldn't find a book"
            ]);
        }
    }

    /**
     * @Route("/book/{id}", methods={"PUT"})
     * @param Request $request
     * @param int $id
     * @param BookEditHandler $handler
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Request $request, int $id, BookEditHandler $handler, EntityManagerInterface $manager): Response
    {
        $book = $manager->find(Book::class, $id);
        if ($book){
            $bookEditCommand = new BookEditCommand();
            $form = $this->createForm(BookEditType::class, $bookEditCommand);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $handler->handle($bookEditCommand, $book);
                $dto = new BookDTO(
                    $book->getId(),
                    $book->getTitle(),
                    $book->getDescription(),
                    $book->getAuthors()->map(
                        function (Author $author) {
                            return new AuthorDTO($author->getId(), $author->getName());
                        }
                    )->getValues(),
                    $book->getStatus()
                );
                return $this->json([
                    'error' => false,
                    'object' => $dto->getArrayData()
                ]);
            }

            $errors = [];
            foreach ($form->getErrors(true) as $error) {
                $errors[] = [$error->getOrigin()->getName() => $error->getMessage()];
            }
        } else {
            $errors = "Couldn't find a book";
        }
        return $this->json(
            [
                'error' => true,
                'errors' => $errors
            ]
        );
    }

    /**
     * @Route("/book/{id}", methods={"DELETE"})
     * @param int $id
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete(int $id, EntityManagerInterface $manager): Response
    {
        $book = $manager->find(Book::class, $id);
        if ($book){
            $book->setInactive();
            $manager->flush();
            $dto = new BookDTO(
                $book->getId(),
                $book->getTitle(),
                $book->getDescription(),
                $book->getAuthors()->map(
                    function (Author $author) {
                        return new AuthorDTO($author->getId(), $author->getName());
                    }
                )->getValues(),
                $book->getStatus()
            );
            return $this->json([
                'error' => false,
                'object' => $dto->getArrayData()
            ]);
        } else {
            return $this->json([
                'error' => true,
                'message' => "Couldn't find a book"
            ]);
        }
    }
}
