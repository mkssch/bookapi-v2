<?php

namespace App\Controller;

use App\DTO\AuthorDTO;
use App\DTO\BookDTO;
use App\DTO\OrderDTO;
use App\Entity\Author;
use App\Repository\OrderRepository;
use App\UseCase\Order\Create\OrderCreateCommand;
use App\UseCase\Order\Create\OrderCreateHandler;
use App\UseCase\Order\Create\OrderCreateType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api")
 */
class OrderController extends AbstractController
{
    /**
     * @Route("/order", methods={"GET"})
     * @param OrderRepository $orderRepository
     * @return JsonResponse
     */
    public function index(OrderRepository $orderRepository): Response
    {
        $orderList = [];
        foreach ($orderRepository->findAll() as $order) {
            $dto = [];
            foreach ($order->getBookList() as $book) {
                $authors = $book->getAuthors()->map(
                    function (Author $author) {
                        return new AuthorDTO($author->getId(), $author->getName());
                    }
                )->getValues();
                $dto[] = new BookDTO($book->getId(), $book->getTitle(), $book->getDescription(), $authors, $book->getStatus());
            }
            $orderDTO = new OrderDTO($order->getId(), $order->getAddress(), $order->getPhone(), $order->getStatus(), $dto);
            $orderList[] = $orderDTO->getArrayData();
        }
        return $this->json($orderList);
    }

    /**
     * @Route("/order", methods={"POST"})
     * @param Request $request
     * @param OrderCreateHandler $handler
     * @return JsonResponse
     */
    public function create(Request $request, OrderCreateHandler $handler)
    {
        $orderCreateCommand = new OrderCreateCommand();
        $form = $this->createForm(OrderCreateType::class, $orderCreateCommand);
        $response = new Response();
        $response->headers = $request->headers;
        try {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $order = $handler->handle($orderCreateCommand);
                return $this->json(
                    [
                        'error' => false,
                        'order_id' => $order->getId()
                    ],
                    Response::HTTP_CREATED
                );
            }
        } catch (\Throwable $exception) {
            return $this->json(
                [
                    'error' => true,
                    'errors' => $exception->getMessage()
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        $errors = [];
        foreach ($form->getErrors(true) as $error){
            $errors[] = [$error->getOrigin()->getName() => $error->getMessage()];
        }
        return $this->json(
            [
                'error' => true,
                'errors' => $errors
            ],
            Response::HTTP_BAD_REQUEST);
    }
}
