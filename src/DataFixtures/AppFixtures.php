<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Order;
use App\Entity\User;
use App\Types\OrderStatus;
use App\Types\Phone;
use App\Types\Status;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $authors = $books = [];
        $status = new Status(Status::STATUS_ACTIVE);
        foreach (range(1, 5) as $id) {
            $author = new Author();
            $author->setName('Author #' . $id);
            $authors[$id] = $author;
            $book = new Book($status);
            $book->setTitle('Book ' . $id);
            $book->setDescription('Some desc...');
            $books[$id] = $book;
            $manager->persist($author);
            $manager->persist($book);
        }

        foreach (range(0, 10) as $counter) {
            $randBook = rand(1, 5);
            $randAuthor = rand(1, 5);
            $books[$randBook]->addAuthor($authors[$randAuthor]);
        }

        $superUser = new User();
        $superUser->setEmail('admin@admin.com');
        $superUser->setPassword('$argon2id$v=19$m=65536,t=4,p=1$6FLtPk2MRQ5UoF6hKS5jSg$Uji4IZdxgcH6rTwS0qnJlORSmxQYVsTNTDnhIdv1Br4');
        $superUser->setRoles(['ROLE_ADMIN']);
        $manager->persist($superUser);

        $phone = new Phone('375291234567');
        foreach (range(0, 3) as $counter) {
            $countBooks = rand(1, 3);
            $order = new Order();
            $order->setAddress('City №' . $counter);
            $order->setPhone($phone);
            array_map(function () use ($books, $order){
                $randBook = rand(1, 5);
                $order->addBook($books[$randBook]);
            }, range(0, $countBooks));

            $order->setStatus(new OrderStatus(
                array_rand(OrderStatus::getStatuses())
            ));
            $order->setCreatedAt(new \DateTimeImmutable('now'));
            $manager->persist($order);
        }

        $manager->flush();
    }
}
