create new book
```
$.ajax({
    type: "POST",
    url: '/api/book/new',
    data: {
        book_create: {
            title: string,
            description: string,
            authorId: author_id
        }
    }
});
```
update book
```
$.ajax({
    type: 'PUT',
    url: '/api/book/2/edit',
    data: {
        title: string,
        description: string,
        author: {
            add: author_id,
            remove: author_id
        },
        status: 1 or 0
    }
})
```
show book
```
$.ajax({
    type: 'GET',
    url: '/api/book/{book_id}'
})
```

delete book
```
$.ajax({
    type: 'DELETE',
    url: '/api/book/{book_id}'
})
```
search books
```
$.ajax({
    type: 'GET',
    url: '/api/book',
    data: { //optional, filtering by criteria
        title: string,
        status: 1 or 0,
        description: string,
        author_name: string
    }
})
```