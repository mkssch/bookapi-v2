<?php


namespace App\Tests\Functional;


use App\Entity\Order;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class JwtTokenTest extends WebTestCase
{
    public function testGetToken(){
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/auth/login',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode(['username' => 'admin@admin.com', 'password' => 'admin'])
        );
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertTrue(isset($data['token']));

        /** @var OrderRepository $repo */
        $repo = $client->getContainer()->get('doctrine')->getRepository(Order::class);
        $count = $repo->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $client->request('GET', '/api/order', [], [], [
            'HTTP_Authorization' => 'Bearer '.$data['token'],
            'HTTP_CONTENT_TYPE' => 'application/json',
        ]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount($count, $data);
    }

}