<?php

namespace Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class AccessTest extends WebTestCase
{
    public function testAccessControl()
    {
        $forbiddenRoutes = [
            'PUT' => '/api/book/1',
            'POST' => '/api/book',
            'DELETE' => '/api/book/1',
            'GET' => '/api/order'
        ];

        $client = static::createClient();
        foreach ($forbiddenRoutes as $method => $route) {
            $client->request($method, $route);
            $this->assertSame(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
        }
    }
}