<?php


namespace App\Tests\Functional;


use App\Entity\Order;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class OrderTest extends WebTestCase
{
    public function testCreate(): void
    {
        $address = 'Minsk';
        $phoneInput = '+375 (29) 124-44-67';
        $phoneOutput = '375291244467';
        $bookList = [1, 3];
        $client = static::createClient();
        $client->request('POST', '/api/order',
            [
                'address' => $address,
                'phone' => $phoneInput,
                'bookList' => $bookList
            ]
        );
        $this->assertSame(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertFalse($data['error']);

        /** @var Order $order */
        $order = $client->getContainer()->get('doctrine')->getRepository(Order::class)->findOneBy([
            'id' => $data['order_id']
        ]);

        $this->assertNotNull($order);
        $this->assertSame($address, $order->getAddress());
        $this->assertSame($phoneOutput, $order->getPhone()->getPhone());
        $this->assertCount(count($bookList), $order->getBookList());
        $this->assertTrue($order->isPending());
    }

    public function testGetOrderList(){
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@admin.com',
            'PHP_AUTH_PW'   => 'admin',
        ]);
        /** @var OrderRepository $repo */
        $repo = $client->getContainer()->get('doctrine')->getRepository(Order::class);
        $count = $repo->createQueryBuilder('t')
            ->select('COUNT(t.id)')
            ->getQuery()
            ->getSingleScalarResult();
        $client->request('GET', '/api/order');
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount($count, $data);
    }

}