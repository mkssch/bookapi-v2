<?php


namespace App\Tests\Functional;


use App\Entity\Book;
use App\Repository\BookRepository;
use App\Types\Status;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class BookTest extends WebTestCase
{
    public function testEditBook()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@admin.com',
            'PHP_AUTH_PW' => 'admin',
        ]);
        /** @var BookRepository $repo */
        $repo = $client->getContainer()->get('doctrine')->getRepository(Book::class);
        $range = $repo->createQueryBuilder('t')
            ->select('MIN(t.id) as minID, MAX(t.id) as maxID')
            ->getQuery()
            ->getSingleResult();
        $randBookID = rand($range['minID'], $range['maxID']);
        $testTitle = "New test title for book №{$randBookID}";
        $testDescription = "New test description for book №{$randBookID}";
        $client->request(
            'PUT',
            "/api/book/{$randBookID}",
            [
                'book_edit' => [
                    'title' => $testTitle,
                    'description' => $testDescription
                ]
            ]
        );
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertFalse($data['error']);
        $book = $repo->find($randBookID);
        $this->assertSame($book->getTitle(), $testTitle);
        $this->assertSame($book->getDescription(), $testDescription);
    }

    public function testSwitchStatusBook()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'admin@admin.com',
            'PHP_AUTH_PW' => 'admin',
        ]);
        /** @var BookRepository $repo */
        $repo = $client->getContainer()->get('doctrine')->getRepository(Book::class);
        $range = $repo->createQueryBuilder('t')
            ->select('MIN(t.id) as minID, MAX(t.id) as maxID')
            ->where('t.status = :active')->setParameter('active', Status::STATUS_ACTIVE)
            ->getQuery()
            ->getSingleResult();
        $randBookID = rand($range['minID'], $range['maxID']);

        //DELETE
        $client->request('DELETE', "/api/book/{$randBookID}");
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertFalse($data['error']);
        $book = $repo->find($randBookID);
        $this->assertTrue($book->isInactive());

        //ENABLE
        $client->request('PUT', "/api/book/{$randBookID}",
            [
                'book_edit' => [
                    'status' => Status::STATUS_ACTIVE
                ]
            ]);
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertFalse($data['error']);
        $book = $repo->find($randBookID);
        $this->assertTrue($book->isActive());
    }

}