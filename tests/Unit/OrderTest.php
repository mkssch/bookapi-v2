<?php

namespace App\Tests\Unit;

use App\Entity\Book;
use App\Types\Status;
use PHPUnit\Framework\TestCase;
use Webmozart\Assert\InvalidArgumentException;

class OrderTest extends TestCase
{
    public function testCreateOrder()
    {
        $order = OrderFactory::createOrder();
        parent::assertTrue($order->isPending());
        $book = new Book(new Status(Status::STATUS_ACTIVE));
        $order->addBook($book);
        parent::assertCount(1, $order->getBookList());
        $order->removeBook($book);
        parent::assertCount(0, $order->getBookList());
    }

    public function testOrderLifeCycle()
    {
        $order = OrderFactory::createOrder();
        parent::assertTrue($order->isPending());
        $order->setProcessed();
        parent::assertTrue($order->isProcessed());
        $order->setDelivered();
        parent::assertTrue($order->isDelivered());
    }

    public function testOrderErrorChangeDeliveredStatus(){
        parent::expectException(InvalidArgumentException::class);
        $order = OrderFactory::createOrder();
        $order->setDelivered();
    }

    public function testOrderErrorChangeDeclinedStatus(){
        parent::expectException(InvalidArgumentException::class);
        $order = OrderFactory::createDeliveredOrder();
        $order->setDeclined();
    }

    public function testDeclineOrder(){
        $order = OrderFactory::createOrder();
        $order->setDeclined();
        parent::assertTrue($order->isDeclined());
    }
}