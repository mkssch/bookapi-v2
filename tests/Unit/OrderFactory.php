<?php

namespace App\Tests\Unit;

use App\Entity\Order;
use App\Types\OrderStatus;
use App\Types\Phone;

class OrderFactory
{
    public static function createOrder(
        ?string             $address = null,
        ?Phone              $phone = null,
        ?OrderStatus        $status = null,
        ?\DateTimeImmutable $dateTimeImmutable = null
    ): Order
    {
        $order = new Order();
        $order->setAddress(is_null($address) ? 'Minsk' : $address);
        $order->setPhone(is_null($address) ? new Phone('+375291234567') : $phone);
        $order->setStatus(is_null($status) ? OrderStatus::pendingStatus() : $status);
        $order->setCreatedAt(is_null($dateTimeImmutable) ? new \DateTimeImmutable('now') : $dateTimeImmutable);
        return $order;
    }

    public static function createDeliveredOrder(
        ?string             $address = null,
        ?Phone              $phone = null,
        ?\DateTimeImmutable $dateTimeImmutable = null
    ): Order
    {
        return self::createOrder(
            $address,
            $phone,
            new OrderStatus(OrderStatus::STATUS_DELIVERED),
            $dateTimeImmutable
        );
    }

}