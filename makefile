load-database: migrations-accept fixtures

up:
	docker-compose up -d

down:
	docker-compose down --remove-orphans

down-clear:
	docker-compose down -v --remove-orphans

migrations-accept:
	docker-compose run --rm php-cli php bin/console doctrine:migrations:migrate -n

fixtures:
	docker-compose run --rm php-cli php bin/console doctrine:fixtures:load -n

tests-run:
	docker-compose run --rm php-cli php bin/phpunit
